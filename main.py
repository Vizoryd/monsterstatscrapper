#Pulls webdriver from Selenium API
from selenium import webdriver
import json
import os

#intializes webdriver
browser = webdriver.Chrome()
try:
    browser.get('https://www.woodus.com/den/games/dq11/monsters.php')

except Exception as e :
    print(e)
#locates elements on page by tagname
elements = browser.find_elements_by_xpath("(//*[@id='datatable']/tbody/tr)")

Monster_Dict = dict()


for element in elements:  # loop through elements
    monster_name = element.find_element_by_name("tname").text  # Store name
    monster_exp = element.find_element_by_name("texp").text  # Store EXP
    monster_locate = element.find_element_by_name("tlocation").text  # Store location
    if Monster_Dict.get(monster_locate) is None:  #if location not in dictionary
        """
        add
        LOCATION {
            NAME: EXP
            }
        """
        Monster_Dict[monster_locate] = {
            monster_name: monster_exp
        }
    else:  # IF location IS in dictionary
        # ADD new values to existing location
        Monster_Dict[monster_locate].update({monster_name: monster_exp})
    # Print to console so we're doing things
    print("Location: ", monster_locate, "\nName: ", monster_name, "\nExp: ", monster_exp, "\n\n")

# Json.dump writes dictionary to file, all red parameters prettify the file
json.dump(Monster_Dict, open(os.path.expanduser("~/Desktop/File.json"), 'w+'), indent=4 , separators= [':',','], sort_keys=False)
browser.close()